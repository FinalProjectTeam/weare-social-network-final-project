Alpha 38 QA - Team 7 - Final project WEare Social Network

In this file you can find overview of our final project, instructions of how the tests can be run and all supporting documentation.

Team members - Erik Kostadinov, Elitsa Vezeva, Maria Ivanova

1. Jira project link - https://weare3.atlassian.net/jira/software/c/projects/WRAR/boards/1/roadmap
2. Project Test plan - https://weare3.atlassian.net/browse/WRAR-202
3. WEare Social Network - https://afternoon-hamlet-44725.herokuapp.com/
    
How to execute the tests:

Preconditions - You have cloned this repository to your local machine.

Postman API tests.
In order to run Postman API requests:
1. Go to 'Postman API tests' folder.
2. Open 'run API tests.bat' file (this executes the whole collection).
* After few minutes, Newman report will be generated in 'newman' folder.
* If you want to run each request separately, you have to import collection and environment to Postman.

Selenium UI tests.
In order to run Selenium UI tests:
1. Go to 'UI automation tests' folder.
2. Double click on the suite that you want to run (grouped by functionality).
* Alternatively, you can run all tests at once by opening 'Run_Suite_all_test_suites.bat' file.
* After tests execution you will find the results in 'target/surefire-reports' folder.

You can find test report in the 'Documentation' folder.
