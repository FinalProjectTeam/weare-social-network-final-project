package test.cases;

import com.telerikacademy.testframework.pages.LoginPage;
import com.telerikacademy.testframework.pages.PostPage;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PostFunctionalityTests extends BaseTestSetup {

    LoginPage loginPage = new LoginPage(actions.getDriver());
    PostPage postPage = new PostPage(actions.getDriver());

    @Before
    public void loginUser() {
        loginPage.loadLoginPage();
        loginPage.loginUserValidCredentials("data.username","data.userPassword");
        loginPage.assertLogoutButtonPresent();
        actions.assertElementPresent("landingpage.personalProfileButton");
    }

    @After
    public void logOutUser() {
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();
    }

    @Test
    public void tc001CreatePublicPost() {
        postPage.createPublicPost();
        String actualString = actions.getTextOfElement("postpage.xpathOriginalText");
        Assert.assertEquals("Post text doesn't match", postPage.textBefore, actualString);
        actions.assertElementPresent("postpage.asserPostPublic");
    }

    @Test
    public void tc002LikePublicPost() {
        postPage.likePublicPost();
        actions.assertElementPresent("postpage.dislikePostButton");
    }

    @Test
    public void tc003DislikePublicPost() {
        postPage.dislikePublicPost();
        actions.assertElementPresent("postpage.likePostButton");
    }

    @Test
    public void tc004EditPublicPost() {
        postPage.editPublicPost();
        String actualString = actions.getTextOfElement("postpage.xpathEditedText");
        Assert.assertEquals("Post text doesn't match", postPage.textAfter, actualString);
    }

    @Test
    public void tc005DeletePublicPost() {
        postPage.deletePublicPost();
        actions.assertElementPresent("postpage.deleteMessage");
    }

}
