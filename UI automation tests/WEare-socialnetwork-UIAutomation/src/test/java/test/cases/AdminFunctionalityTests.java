package test.cases;

import com.telerikacademy.testframework.pages.AdminPage;
import com.telerikacademy.testframework.pages.LoginPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdminFunctionalityTests extends BaseTestSetup {

    AdminPage adminPage = new AdminPage(actions.getDriver());
    LoginPage loginPage = new LoginPage(actions.getDriver());

    @Before
    public void loginAdminNavigateToAdminZoneAndViewUsers() {
        loginPage.loadLoginPage();
        loginPage.loginUserValidCredentials("data.adminUserName", "data.adminPassword");
        actions.assertElementPresent("loginpage.logOutButton");
        actions.assertElementPresent("landingpage.personalProfileButton");
        actions.assertElementPresent("adminpart.adminZoneButton");
        adminPage.goToAdminZone();
        actions.assertElementPresent("adminpart.viewUsersButton");
        adminPage.viewAllUsers();
        actions.assertElementPresent("adminpart.seeProfileButton");
    }

    @After
    public void logOutAdmin(){
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();
    }

    @Test
    public void tc001DisableUserProfile() {
        adminPage.findUserAndNavigateToProfile();
        actions.assertElementPresent("adminpart.userProfilePic");
        adminPage.disableProfile();
        actions.assertElementPresent("adminpart.enableButton");
    }

    @Test
    public void tc002EnableUserProfile() {
        adminPage.findUserAndNavigateToProfile();
        actions.assertElementPresent("adminpart.userProfilePic");
        adminPage.enableProfile();
        actions.assertElementPresent("adminpart.disableButton");
    }

}
