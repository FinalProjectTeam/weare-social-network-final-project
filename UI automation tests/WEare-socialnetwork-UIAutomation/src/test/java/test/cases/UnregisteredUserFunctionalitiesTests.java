package test.cases;

import com.telerikacademy.testframework.pages.UnregisteredUserPage;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UnregisteredUserFunctionalitiesTests extends BaseTestSetup {

    UnregisteredUserPage unregisteredUserPage = new UnregisteredUserPage(actions.getDriver());

    @After
    public void returnToHomePage() {

        actions.waitForElementClickable("homepage.homeButton");
        actions.clickElement("homepage.homeButton");

    }

    @Test
    public void tc001VisitAboutUsPage() {

        unregisteredUserPage.visitAboutUsPage();
        unregisteredUserPage.assertAboutUsPageReached();
    }

    @Test
    public void tc002VisitLatestPublicPosts() {

        unregisteredUserPage.visitPublicPostsPage();
        unregisteredUserPage.assertLatestPostsReached();
    }

    @Test
    public void tc003SearchUserByProfession() {

        unregisteredUserPage.searchUserByProfession();
        unregisteredUserPage.assertUserIsPresent();
    }

}
