package test.cases;

import com.telerikacademy.testframework.pages.LoginPage;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class LoginFunctionalityTests extends BaseTestSetup {

    LoginPage loginPage = new LoginPage(actions.getDriver());

    @Before
    public void navigateToHomePageAndLoginPage() {
        loginPage.loadLoginPage();
        actions.assertElementPresent("loginpage.loginButton");

    }

    @Test
    public void tc001LoginUserWithInvalidUsername() {
        loginPage.loginUserInvalidUsername();
        loginPage.assertLoginErrorMessagePresent();
    }

    @Test
    public void tc002LoginUserInvalidPassword() {

        loginPage.loginUserInvalidPassword();
        loginPage.assertLoginErrorMessagePresent();
    }

    @Test
    public void tc003LoginUserValidInput() {

        loginPage.loginUserValidCredentials("data.username","data.userPassword");
        loginPage.assertLogoutButtonPresent();
        loginPage.assertPersonalProfileButtonPresent();
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();
    }

    @Test
    public void tc004LogoutUserSuccessfully(){

        loginPage.loginUserValidCredentials("data.username","data.userPassword");
        loginPage.assertLogoutButtonPresent();
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();

    }

}
