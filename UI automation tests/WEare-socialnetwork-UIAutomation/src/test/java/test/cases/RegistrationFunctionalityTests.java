package test.cases;

import com.telerikacademy.testframework.pages.RegistrationPage;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegistrationFunctionalityTests extends BaseTestSetup {

    RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());

    @Before
    public void loadRegisterPage() {
        actions.waitForElementClickable("homepage.registerButton");
        actions.clickElement("homepage.registerButton");
    }

    @After
    public void returnHome() {
        actions.waitForElementClickable("registration.homeButton");
        actions.clickElement("registration.homeButton");
    }


    @Test
    public void tc001RegisterUserValidInput() {

        registrationPage.registerUser();
        actions.assertElementPresent("registration.welcomeMessage");
        actions.assertElementPresent("registration.updateProfileOption");
    }

    @Test
    public void tc002RegisterUserWithInvalidEmail() {

        registrationPage.registerUserInvalidEmail();
        registrationPage.assertInvalidEmailMessagePresent();
    }

    @Test
    public void tc003RegisterUserWithInvalidPassword() {

        registrationPage.registerUserInvalidPassword();
        registrationPage.assertInvalidPasswordMessagePresent();
    }

    @Test
    public void tc004RegisterUserWithInvalidUsername() {

        registrationPage.registerUserInvalidUsername();
        registrationPage.assertInvalidUserNameMessagePresent();

    }

}
