package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebDriver;

public class AdminPage extends BasePage {

    public AdminPage(WebDriver driver) {super(driver, "home.page");}

    public void goToAdminZone() {
        actions.waitForElementClickable("adminpart.adminZoneButton");
        actions.clickElement("adminpart.adminZoneButton");
    }

    public void viewAllUsers() {
        actions.waitForElementClickable("adminpart.viewUsersButton");
        actions.clickElement("adminpart.viewUsersButton");
    }

    public void findUserAndNavigateToProfile() {
        actions.waitForElementClickable("adminpart.seeProfileButton");
        actions.clickElement("adminpart.seeProfileButton");
    }

    public void disableProfile() {
        actions.waitForElementClickable("adminpart.disableButton");
        actions.clickElement("adminpart.disableButton");

    }
    public void enableProfile() {
        actions.waitForElementClickable("adminpart.enableButton");
        actions.clickElement("adminpart.enableButton");
    }

}
