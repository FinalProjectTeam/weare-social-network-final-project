package com.telerikacademy.testframework.pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;
import java.util.Random;

public class RegistrationPage extends BasePage {
    public RegistrationPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void registerUser() {

        actions.typeValueInField(generateWord(6), "registration.name");
        actions.typeValueInField(generateWord(5) + "@gmail.com", "registration.email");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.password");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.confirmPassword");
        actions.waitForElementClickable("registration.registerButton");
        actions.clickElement("registration.registerButton");
    }

    public void registerUserInvalidEmail(){

        actions.typeValueInField(generateWord(6), "registration.name");
        actions.typeValueInField(generateWord(5), "registration.email");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.password");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.confirmPassword");
        actions.waitForElementClickable("registration.registerButton");
        actions.clickElement("registration.registerButton");
    }

    public void registerUserInvalidPassword(){

        String password = generateWord(1);
        actions.typeValueInField(generateWord(6), "registration.name");
        actions.typeValueInField(generateWord(5) + "@gmail.com", "registration.email");
        actions.typeValueInField(password, "registration.password");
        actions.typeValueInField(password, "registration.confirmPassword");
        actions.waitForElementClickable("registration.registerButton");
        actions.clickElement("registration.registerButton");
    }

    public void registerUserInvalidUsername(){

        actions.typeValueInField(generateWord(6) + "1", "registration.name");
        actions.typeValueInField(generateWord(5) + "@gmail.com", "registration.email");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.password");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.password"), "registration.confirmPassword");
        actions.waitForElementClickable("registration.registerButton");
        actions.clickElement("registration.registerButton");

    }

    public static String generateWord(int length) {

        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                + "lmnopqrstuvwxyz";

        Random random = new Random();
        StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            builder.append(chars.charAt(random.nextInt(chars.length())));

        }
        return builder.toString();

    }

    public void assertInvalidEmailMessagePresent(){
        actions.assertElementPresent("registration.invalidEmailMessage");
    }

    public void assertInvalidPasswordMessagePresent(){
        actions.assertElementPresent("registration.invalidPasswordMessage");
    }

    public void assertInvalidUserNameMessagePresent(){
        actions.assertElementPresent("registration.invalidUserNameMessage");
    }

}
