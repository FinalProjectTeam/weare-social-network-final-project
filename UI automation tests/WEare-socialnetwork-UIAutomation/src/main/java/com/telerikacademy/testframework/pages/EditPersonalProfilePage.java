package com.telerikacademy.testframework.pages;

import org.checkerframework.checker.index.qual.PolyUpperBound;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.RegistrationPage.generateWord;

public class EditPersonalProfilePage extends BasePage {

    public static final String MESSAGE = "%s don't match";
    public String editedFullName;
    public String newBio;

    public EditPersonalProfilePage(WebDriver driver) {
        super (driver, "home.page");
    }

    public void navigateToEditProfilePage() {
        actions.waitForElementClickable("editPersonalProfilePage.personalProfileButton");
        actions.clickElement("editPersonalProfilePage.personalProfileButton");
        actions.waitForElementClickable("editPersonalProfilePage.editProfileButton");
        actions.clickElement("editPersonalProfilePage.editProfileButton");

    }

    public void editName() {
        actions.clearTextField("editPersonalProfilePage.editFirstNameField");
        String firstName = generateWord(6);
        actions.typeValueInField(firstName, "editPersonalProfilePage.editFirstNameField");
        actions.clearTextField("editPersonalProfilePage.editLastNameField");
        String lastName = generateWord(8);
        actions.typeValueInField(lastName, "editPersonalProfilePage.editLastNameField");
        editedFullName = firstName + " " + lastName;
        actions.typeValueInField("01-01-1999", "editPersonalProfilePage.editBirthdayField");
        actions.clickElement("editPersonalProfilePage.updateMyProfileButton");
    }

    public void editBio() {
        actions.clearTextField("editPersonalProfilePage.editBioField");
        newBio = generateWord(15);
        actions.typeValueInField(newBio, "editPersonalProfilePage.editBioField");
        actions.clickElement("editPersonalProfilePage.updateMyProfileButton");
    }

    public void assertNameMatches() {
        actions.waitForElementClickable("editPersonalProfilePage.personalProfileButton");
        actions.clickElement("editPersonalProfilePage.personalProfileButton");
        String actualFullName = actions.getTextOfElement("editPersonalProfilePage.userName");
        Assert.assertEquals(String.format(MESSAGE, "Name"), editedFullName, actualFullName);
    }

    public void assertBioMatches() {
        actions.waitForElementClickable("editPersonalProfilePage.personalProfileButton");
        actions.clickElement("editPersonalProfilePage.personalProfileButton");
        String actualBio = actions.getTextOfElement("editPersonalProfilePage.bio");
        Assert.assertEquals(String.format(MESSAGE, "Bio"), newBio, actualBio);
    }
}
