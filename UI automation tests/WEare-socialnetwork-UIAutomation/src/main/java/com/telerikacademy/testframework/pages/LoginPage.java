package com.telerikacademy.testframework.pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loadLoginPage() {
        actions.waitForElementClickable("homepage.signInButton");
        actions.clickElement("homepage.signInButton");

    }

    public void loginUserInvalidUsername() {
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.wrongUsername"), "loginpage.username");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.userPassword"), "loginpage.password");
        actions.clickElement("loginpage.loginButton");

    }

    public void loginUserInvalidPassword() {
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.username"), "loginpage.username");
        actions.typeValueInField(Utils.getConfigPropertyByKey("data.wrongPassword"), "loginpage.password");
        actions.clickElement("loginpage.loginButton");
    }

    public void loginUserValidCredentials(String username, String password) {
        actions.typeValueInField(Utils.getConfigPropertyByKey(username), "loginpage.username");
        actions.typeValueInField(Utils.getConfigPropertyByKey(password), "loginpage.password");
        actions.clickElement("loginpage.loginButton");
    }

    public void logoutUser(){
        actions.clickElement("registration.homeButton");
        actions.clickElement("loginpage.logOutButton");
    }

    public void assertLoginErrorMessagePresent() {
        actions.waitForElementVisible("loginpage.errorText");
        actions.assertElementPresent("loginpage.errorText");
    }

    public void assertLogoutButtonPresent() {
        actions.waitForElementVisible("loginpage.logOutButton");
        actions.assertElementPresent("loginpage.logOutButton");
    }

    public void assertPersonalProfileButtonPresent() {
        actions.waitForElementVisible("landingpage.personalProfileButton");
        actions.assertElementPresent("landingpage.personalProfileButton");
    }

    public void assertLogoutMessagePresent(){
        actions.assertElementPresent("loginpage.logoutText");
    }

}
