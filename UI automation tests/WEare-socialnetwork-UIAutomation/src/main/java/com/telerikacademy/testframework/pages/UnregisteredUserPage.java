package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebDriver;

public class UnregisteredUserPage extends BasePage {

    public static final String DEFAULT_OCCUPATION = "Chef/Cook";

    public UnregisteredUserPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void visitAboutUsPage() {

        actions.waitForElementClickable("homepage.aboutUsButton");
        actions.clickElement("homepage.aboutUsButton");
    }

    public void visitPublicPostsPage() {
        actions.waitForElementClickable("homepage.latestPostsButton");
        actions.clickElement("homepage.latestPostsButton");
    }

    public void searchUserByProfession() {

        actions.typeValueInField(DEFAULT_OCCUPATION, "homepage.searchFieldProfession");
        actions.clickElement("homepage.searchButton");

    }


    public void assertAboutUsPageReached() {

        actions.waitForElementVisible("homepage.aboutUsSign");
        actions.assertElementPresent("homepage.aboutUsSign");
    }

    public void assertLatestPostsReached() {
        actions.waitForElementVisible("homepage.exploreAllPostsSign");
        actions.assertElementPresent("homepage.exploreAllPostsSign");
    }

    public void assertUserIsPresent() {
        actions.waitForElementVisible("homepage.userProfile");
        actions.assertElementPresent("homepage.userProfile");
    }
}
